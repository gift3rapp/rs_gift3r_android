package com.richestsoft.gift3r.models.interactors;

import com.richestsoft.gift3r.models.networkrequest.NetworkRequestCallbacks;
import com.richestsoft.gift3r.models.networkrequest.RestClient;
import com.richestsoft.gift3r.utils.ApplicationGlobal;

import io.reactivex.disposables.Disposable;

/**
 * Created by shray on 09-02-2018.
 */

public class SettingsInteractor extends BaseInteractor {

    public Disposable changePassword(String oldPassword, String newPassword,
                                     NetworkRequestCallbacks networkRequestCallbacks) {

        return getDisposable(RestClient.get().changePassword(ApplicationGlobal.getSessionId(),
                oldPassword, newPassword, ApplicationGlobal.getDeviceLocale()),
                networkRequestCallbacks);
    }

    public Disposable logout(NetworkRequestCallbacks networkRequestCallbacks) {

        return getDisposable(RestClient.get().logout(ApplicationGlobal.getSessionId(),
                ApplicationGlobal.getDeviceLocale()), networkRequestCallbacks);
    }

}
