package com.richestsoft.gift3r.models.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyFavourites_Response {

    @SerializedName("favouritelist")
    @Expose
    private List<Favouritelist> favouritelist = null;
    @SerializedName("liststatus")
    @Expose
    private Integer liststatus;
    @SerializedName("profile")
    @Expose
    private Profile profile;

    public List<Favouritelist> getFavouritelist() {
        return favouritelist;
    }

    public void setFavouritelist(List<Favouritelist> favouritelist) {
        this.favouritelist = favouritelist;
    }

    public Integer getListstatus() {
        return liststatus;
    }

    public void setListstatus(Integer liststatus) {
        this.liststatus = liststatus;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

}
