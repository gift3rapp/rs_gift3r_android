package com.richestsoft.gift3r.models.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by user28 on 8/2/18.
 */

public class Store implements Parcelable {

    private String name = "";
    private String image = "";
    private String website = "";
    private String phone_no = "";
    private String address = "";
    private Integer store_id;
    private Integer status;
    private ArrayList<GiftCard> available_cards = new ArrayList<>();

    public Store() {
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getWebsite() {
        return website;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public String getAddress() {
        return address;
    }

    public Integer getStore_id() {
        return store_id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public ArrayList<GiftCard> getAvailable_cards() {
        return available_cards;
    }

    protected Store(Parcel in) {
        name = in.readString();
        image = in.readString();
        website = in.readString();
        phone_no = in.readString();
        address = in.readString();
        status = in.readInt();
        store_id = in.readByte() == 0x00 ? null : in.readInt();
        if (in.readByte() == 0x01) {
            available_cards = new ArrayList<GiftCard>();
            in.readList(available_cards, GiftCard.class.getClassLoader());
        } else {
            available_cards = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(image);
        dest.writeString(website);
        dest.writeString(phone_no);
        dest.writeString(address);
        dest.writeInt(status);
        if (store_id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(store_id);
        }
        if (available_cards == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(available_cards);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Store> CREATOR = new Parcelable.Creator<Store>() {
        @Override
        public Store createFromParcel(Parcel in) {
            return new Store(in);
        }

        @Override
        public Store[] newArray(int size) {
            return new Store[size];
        }
    };
}
