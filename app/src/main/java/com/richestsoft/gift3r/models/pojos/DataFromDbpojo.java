package com.richestsoft.gift3r.models.pojos;

public class DataFromDbpojo {


    private String id;
    private String jobnumber;
    private String jobdescription;
    private String jobdate;
    private String ndtmethod;
    private String ToatalTime;
    private String dailyworkHr;
    private String completedTime;
    private String notes;
    private String info;
    private String preferences;
    private String title;
    private Boolean checked=false;

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public byte[] image;

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getPreferences() {
        return preferences;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setPreferences(String preferences) {
        this.preferences = preferences;
    }

    public String getCompletedTime() {
        return completedTime;
    }

    public void setCompletedTime(String completedTime) {
        this.completedTime = completedTime;
    }

    public DataFromDbpojo() {
    }

    public DataFromDbpojo(int i, String string, byte[] blob) {

    }

    public DataFromDbpojo(int i, String string, String blob) {

    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }






    public String getDailyworkHr() {
        return dailyworkHr;
    }

    public void setDailyworkHr(String dailyworkHr) {
        this.dailyworkHr = dailyworkHr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobnumber() {
        return jobnumber;
    }

    public void setJobnumber(String jobnumber) {
        this.jobnumber = jobnumber;
    }

    public String getJobdescription() {
        return jobdescription;
    }

    public void setJobdescription(String jobdescription) {
        this.jobdescription = jobdescription;
    }

    public String getJobdate() {
        return jobdate;
    }

    public void setJobdate(String jobdate) {
        this.jobdate = jobdate;
    }

    public String getNdtmethod() {
        return ndtmethod;
    }

    public void setNdtmethod(String ndtmethod) {
        this.ndtmethod = ndtmethod;
    }

    public String getToatalTime() {
        return ToatalTime;
    }

    public void setToatalTime(String toatalTime) {
        this.ToatalTime = toatalTime;
    }
}
  
   
