package com.richestsoft.gift3r.models.interactors;

import com.richestsoft.gift3r.models.networkrequest.NetworkRequestCallbacks;
import com.richestsoft.gift3r.models.networkrequest.RestClient;
import com.richestsoft.gift3r.utils.ApplicationGlobal;
import com.richestsoft.gift3r.utils.Constants;
import com.richestsoft.gift3r.utils.GeneralFunctions;

import io.reactivex.disposables.Disposable;

/**
 * Created by user28 on 13/2/18.
 */

public class SignInInteractor extends BaseInteractor {

    public Disposable signInUser(String email, String password,
                                 NetworkRequestCallbacks networkRequestCallbacks) {

        return getDisposable(RestClient.get().signIn(email, password,
                GeneralFunctions.generateRandomString(15), Constants.DEVICE_TYPE,
                ApplicationGlobal.getFirebasetoken(),
                ApplicationGlobal.getDeviceLocale()), networkRequestCallbacks);
    }


    public Disposable forgotPassword(String email, NetworkRequestCallbacks networkRequestCallbacks) {

        return getDisposable(RestClient.get().forgotPassword(email,
                ApplicationGlobal.getDeviceLocale()), networkRequestCallbacks);
    }

}
