package com.richestsoft.gift3r.models.pojos;

/**
 * Created by Mukesh on 14/07/2016.
 */
public class Country {

    public String name = "";
    public String dial_code = "";
    public String code = "";

    public String getName() {
        return name;
    }

    public String getDial_code() {
        return dial_code;
    }

    public String getCode() {
        return code;
    }
}
