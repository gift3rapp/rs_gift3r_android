package com.richestsoft.gift3r.models.pojos;

/**
 * Created by Mukesh on 15/03/2017.
 */

public class PojoNetworkResponse {

    private boolean isSuccess;
    private boolean isSessionExpired;

    public PojoNetworkResponse(boolean isSuccess, boolean isSessionExpired) {
        this.isSuccess = isSuccess;
        this.isSessionExpired = isSessionExpired;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public boolean isSessionExpired() {
        return isSessionExpired;
    }
}
