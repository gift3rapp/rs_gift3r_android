package com.richestsoft.gift3r.models.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payment_Request {

    @SerializedName("session_id")
    @Expose
    private String sessionId;
    @SerializedName("card_number")
    @Expose
    private String cardNumber;
    @SerializedName("card_type")
    @Expose
    private String cardType;
    @SerializedName("expiry_date")
    @Expose
    private String expiryDate;
    @SerializedName("cvv")
    @Expose
    private String cvv;
    @SerializedName("amount")
    @Expose
    private String amount;

    @SerializedName("card_holdername")
    @Expose
    private String cardHoldername;

    @SerializedName("card_id")
    @Expose
    private String cardId;


    @SerializedName("zipcode")
    @Expose
    private String zipcode;

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }



    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCardHoldername() {
        return cardHoldername;
    }

    public void setCardHoldername(String cardHoldername) {
        this.cardHoldername = cardHoldername;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

}
