package com.richestsoft.gift3r.models.pojos;

/**
 * Created by Mukesh on 31/08/2016.
 */
public class PojoCommon {

    private String message = "";
    private int status;

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }
}
