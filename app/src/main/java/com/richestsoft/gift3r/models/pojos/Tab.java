package com.richestsoft.gift3r.models.pojos;

import android.app.Fragment;

/**
 * Created by Mukesh on 18/06/2016.
 */
public class Tab {

    private Fragment tabFragment;
    private String tabName = "";
    private int tabIcon;
    private boolean isShowTabName;

    public Tab(Fragment tabFragment, String tabName, int tabIcon, boolean isShowTabName) {
        this.tabFragment = tabFragment;
        this.tabName = tabName;
        this.tabIcon = tabIcon;
        this.isShowTabName = isShowTabName;
    }

    public Fragment getTabFragment() {
        return tabFragment;
    }

    public String getTabName() {
        return tabName;
    }

    public int getTabIcon() {
        return tabIcon;
    }

    public boolean isShowTabName() {
        return isShowTabName;
    }
}
