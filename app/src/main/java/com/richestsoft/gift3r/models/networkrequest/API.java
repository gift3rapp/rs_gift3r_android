package com.richestsoft.gift3r.models.networkrequest;

import com.richestsoft.gift3r.models.pojos.Contact_Status_Request;
import com.richestsoft.gift3r.models.pojos.Contact_Status_Response;
import com.richestsoft.gift3r.models.pojos.Get_User_Detail;
import com.richestsoft.gift3r.models.pojos.GiftCard;
import com.richestsoft.gift3r.models.pojos.Invite_Request;
import com.richestsoft.gift3r.models.pojos.Invite_Response;
import com.richestsoft.gift3r.models.pojos.MyFavourites_Response;
import com.richestsoft.gift3r.models.pojos.Payment_Request;
import com.richestsoft.gift3r.models.pojos.Payment_Response;
import com.richestsoft.gift3r.models.pojos.PojoCommon;
import com.richestsoft.gift3r.models.pojos.PojoUserSignIn;
import com.richestsoft.gift3r.models.pojos.Store;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Mukesh on 06-06-2016.
 */

public interface API {

    //login
    @FormUrlEncoded
    @POST("login")
    Observable<Response<PojoUserSignIn>> signIn(@Field("email") String email,
                                                @Field("password") String password,
                                                @Field("device_id") String deviceId,
                                                @Field("device_type") String deviceType,
                                                @Field("device_token") String deviceToken,
                                                @Field("locale") String locale);

    //get otp
    @FormUrlEncoded
    @POST("otp")
    Observable<Response<PojoCommon>> getOtp(@Field("country_code") String countryCode,
                                            @Field("phone_number") String phoneNumber,
                                            @Field("email") String email,
                                            @Field("locale") String locale);

    //SignUp
    @FormUrlEncoded
    @POST("signup")
    Observable<Response<PojoUserSignIn>> signUp(@Field("email") String email,
                                                @Field("country_code") String countryCode,
                                                @Field("phone_number") String phoneNumber,
                                                @Field("name") String name,
                                                @Field("password") String password,
                                                @Field("otp") String otp,
                                                @Field("device_id") String deviceId,
                                                @Field("device_type") String deviceType,
                                                @Field("device_token") String deviceToken,
                                                @Field("locale") String locale);

    //Change Password
    @FormUrlEncoded
    @POST("change-password")
    Observable<Response<PojoCommon>> changePassword(@Field("session_id") String sessionId,
                                                    @Field("password") String oldPassword,
                                                    @Field("new_password") String newPassword,
                                                    @Field("locale") String locale);

    //User Logout
    @FormUrlEncoded
    @POST("logout")
    Observable<Response<PojoCommon>> logout(@Field("session_id") String sessionId,
                                            @Field("locale") String locale);

    //Forgot Password
    @FormUrlEncoded
    @POST("forgot-password")
    Observable<Response<PojoCommon>> forgotPassword(@Field("email") String email,
                                                    @Field("locale") String locale);

    @GET("store-list")
    Observable<Response<ArrayList<Store>>> getStoreList(@Query("session_id") String sessionId,
                                                        @Query("search") String searchKey,
                                                        @Query("limit") Integer pageLimit,
                                                        @Query("page") Integer page,
                                                        @Query("locale") String locale);

    @GET("store")
    Observable<Response<Store>> getStore(@Query("session_id") String sessionId,
                                         @Query("store_id") String storeId,
                                         @Query("locale") String locale);

    @GET("my-cards")
    Observable<Response<ArrayList<GiftCard>>> getMyCards(@Query("session_id") String sessionId,
                                                         @Query("search") String searchKey,
                                                         @Query("locale") String locale);

    @FormUrlEncoded
    @POST("redeem-cards")
    Observable<Response<PojoCommon>> redeemCard(@Field("session_id") String sessionId,
                                                @Field("card_id") Integer cardId,
                                                @Field("mypurchasedcard_id") String purchasecardid,
                                                @Field("receipt_number") String receiptNumber,
                                                @Field("bill_amount") String billAmount,
                                                @Field("amount") String redeemedAmount,
                                                @Field("locale") String locale,
                                                @Field("assist_name") String redem_name);

    @FormUrlEncoded
    @POST("send-cards")
    Observable<Response<PojoCommon>> sendCard(@Field("session_id") String sessionId,
                                              @Field("card_id") Integer cardId,
                                              @Field("mypurchasedcard_id") String purchasecardid,
                                              @Field("phone_number") String phoneNumber,
                                              @Field("locale") String locale,
                                              @Field("message") String message);

    @FormUrlEncoded
    @POST("buy-cards")
    Observable<Response<PojoCommon>> buyCard(@Field("session_id") String sessionId,
                                             @Field("card_id") Integer cardId,
                                             @Field("phone_number") String phoneNumber,
                                             @Field("locale") String locale,
                                             @Field("message") String message,
                                             @Field("payment_id") int payment_id);


 /*   @FormUrlEncoded
    @POST("favourite-store")
    Observable<Response<PojoCommon>> AddFavorite(@Field("session_id") String sessionId,
                                             @Field("store_id") String store_id);
*/

    @FormUrlEncoded
    @POST("favourite-store")
    Call<PojoCommon> AddFavorite(@Field("session_id") String sessionId,
                                    @Field("store_id") String store_id);

    @FormUrlEncoded
    @POST("myfavourite-storelist")
    Call<List<Store>> My_favourites(@Field("session_id") String sessionId);

    @FormUrlEncoded
    @POST("favourite-storelist")
    Call<Get_User_Detail> User_ProfieDetail(@Field("session_id") String sessionId,
                                            @Field("phone_number") String phone_number);

    @POST("contact-list")
    Call<Contact_Status_Response> GetcontactStatus(@Header("Content-Type") String content_Type,
                                           @Body Contact_Status_Request contact_status_request);

    @POST("payment")
    Call<Payment_Response> Payment(@Header("Content-Type") String content_Type, @Body Payment_Request payment_request);


 @POST("sendappurl")
    Call<Invite_Response> invite_method(@Header("Content-Type") String content_Type,
                                  @Body Invite_Request invite_request);

    @FormUrlEncoded
    @POST("checknumberstatus")
    Call<Get_User_Detail> checknumberstatus(@Field("country_code") String sessionId,
                                            @Field("phone_number") String phone_number);



}
