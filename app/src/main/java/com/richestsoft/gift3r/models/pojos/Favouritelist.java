package com.richestsoft.gift3r.models.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Favouritelist {

    @SerializedName("favouritelist")
    @Expose
    private User_Favorite_List user_favorite_list;

    public User_Favorite_List getUser_favorite_list() {
        return user_favorite_list;
    }

    public void setUser_favorite_list(User_Favorite_List user_favorite_list) {
        this.user_favorite_list = user_favorite_list;
    }
}
