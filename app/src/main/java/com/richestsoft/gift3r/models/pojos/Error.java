package com.richestsoft.gift3r.models.pojos;

/**
 * Created by Mukesh on 15/03/2016.
 */
public class Error {

    private String error = "";
    private String error_description = "";

    public String getError() {
        return error;
    }

    public String getError_description() {
        return error_description;
    }

}