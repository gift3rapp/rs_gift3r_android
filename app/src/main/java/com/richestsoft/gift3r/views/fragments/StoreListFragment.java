package com.richestsoft.gift3r.views.fragments;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.pojos.Store;
import com.richestsoft.gift3r.presenters.BasePresenter;
import com.richestsoft.gift3r.presenters.StoreListPresenter;
import com.richestsoft.gift3r.views.StoreListView;
import com.richestsoft.gift3r.views.adapters.StoreListAdapter;
import com.richestsoft.gift3r.views.interfaces.LoadMoreListener;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by user28 on 8/2/18.
 */

public class StoreListFragment extends BaseRecyclerViewFragment implements StoreListView, LoadMoreListener {

    @BindView(R.id.searchView) SearchView searchView;

    public static final int STORES_PAGE_LIMIT = 20;

    private StoreListPresenter mStoreListPresenter;
    private StoreListAdapter mStoreListAdapter;

    private int page = 1;
    private String searchKey = "";

    @Override public int getLayoutId() {
        return R.layout.fragment_store_list;
    }

    @Override public void setData() {

        //initialize and attach presenter
        mStoreListPresenter = new StoreListPresenter();
        mStoreListPresenter.attachView(this);

        // fetch store list
        mStoreListPresenter.fetchMyCardList(true, searchKey, page);

        searchView.setQueryHint(getString(R.string.store_list_search_view_hint));
        // set query submit listener
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                searchKey = s;
                page = 1;
                mStoreListPresenter.fetchMyCardList(true, searchKey, page);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.isEmpty()) {
                    searchKey = "";
                    page = 1;
                    mStoreListPresenter.fetchMyCardList(true, searchKey, page);
                }
                return false;
            }
        });
    }

    @Override public RecyclerView.LayoutManager getLayoutManager() {
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return mStoreListAdapter.ROW_TYPE_LOADER == mStoreListAdapter
                        .getItemViewType(position) ? gridLayoutManager.getSpanCount() : 1;
            }
        });
        return gridLayoutManager;
    }

    @Override public RecyclerView.Adapter getRecyclerViewAdapter() {
        if (null == mStoreListAdapter) {
            mStoreListAdapter = new StoreListAdapter(this);
        }
        return mStoreListAdapter;
    }

    @Override public void updateRecyclerViewData(ArrayList<?> dataList) {
        mStoreListAdapter.updateList((ArrayList<Store>) dataList, page);
    }

    @Override public boolean isShowRecyclerViewDivider() {
        return false;
    }

    @Override public void onPullDownToRefresh() {
        page = 1;
        mStoreListPresenter.fetchMyCardList(true, searchKey, page);
    }

    @Override public BasePresenter getPresenter() {
        return mStoreListPresenter;
    }

    @Override public void onLoadMore() {
        page++;
        mStoreListPresenter.fetchMyCardList(true, searchKey, page);
    }
}
