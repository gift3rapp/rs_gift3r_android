package com.richestsoft.gift3r.views.fragments;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.presenters.BasePresenter;
import com.richestsoft.gift3r.presenters.SignUpPresenter;
import com.richestsoft.gift3r.utils.Constants;
import com.richestsoft.gift3r.utils.GeneralFunctions;
import com.richestsoft.gift3r.views.SignUpView;
import com.richestsoft.gift3r.views.adapters.CountriesListAdapter;
import com.richestsoft.gift3r.views.dialogfragments.CountryCodesDialogFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by user28 on 6/2/18.
 */

public class SignUpFragment extends BaseFragment implements SignUpView {

    @BindView(R.id.etPassword) EditText etPassword;
    @BindView(R.id.etPhoneNumber) EditText etPhoneNumber;
    @BindView(R.id.etName) EditText etName;
    @BindView(R.id.etEmail) EditText etEmail;
    @BindView(R.id.tvCode) TextView tvCode;
    private SignUpPresenter mSignUpPresenter;

    @Override public int getLayoutId() {
        return R.layout.fragment_sign_up;
    }

    @Override public void init() {
        // formatting toolbar and set toolbar title
        tvTitle.setText(R.string.fragment_title_sign_up);
        toolbar.setBackground(null);
        tvTitle.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryText));
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black);

        //initialize and attach presenter
        mSignUpPresenter = new SignUpPresenter();
        mSignUpPresenter.attachView(this);
    }

    @Override public BasePresenter getPresenter() {
        return mSignUpPresenter;
    }

    @OnClick({R.id.btnSignUp, R.id.tvCode})
    public void onClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSignUp:
                mSignUpPresenter.signUpUser(etEmail.getText().toString().trim(), tvCode.getText().toString().trim(), etPhoneNumber.getText().toString().trim(), etName.getText().toString().trim(), etPassword.getText().toString().trim());
                break;
            case R.id.tvCode:
                CountryCodesDialogFragment codesDialogFragment = new CountryCodesDialogFragment();
                codesDialogFragment.setTargetFragment(this, Constants.REQUEST_CODE_COUNTRY_CODE);
                codesDialogFragment.show(getFragmentManager(), getString(R.string.dialog));
                break;
        }
    }

    // set country code result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Activity.RESULT_OK == resultCode && Constants.REQUEST_CODE_COUNTRY_CODE == requestCode
                && null != data) {
            tvCode.setText(data.getStringExtra(CountriesListAdapter.INTENT_EXTRAS_COUNTRY_CODE));
        }
    }

    @Override public void navigateToVerification() {
        GeneralFunctions.replaceFragmentWithBackStack(getFragmentManager(),
                R.animator.slide_right_in, 0, 0,
                R.animator.slide_right_out, OtpVerificationFragment.newInstance(etEmail.getText().toString().trim(), etPassword.getText().toString().trim(), etPhoneNumber.getText().toString().trim(), etName.getText().toString().trim(), tvCode.getText().toString().trim()), null,
                R.id.clFragContainerMain);
    }
}
