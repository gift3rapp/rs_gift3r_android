package com.richestsoft.gift3r.views.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.presenters.BasePresenter;
import com.richestsoft.gift3r.presenters.RedeemCardConfirmationPresenter;
import com.richestsoft.gift3r.views.RedeemCardConfirmationView;

import java.text.NumberFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by user28 on 12/2/18.
 */

public class RedeemCardConfirmationFragment extends BaseFragment implements RedeemCardConfirmationView, TextWatcher {

    @BindView(R.id.tvPartialAmountText) TextView tvPartialAmountText;
    @BindView(R.id.tvTotalAmount) TextView tvTotalAmount;
    @BindView(R.id.etAmount) EditText etAmount;
    //@BindView(R.id.etName) EditText etName;
    private String current = "";
    public static final String BUNDLE_EXTRAS_CARD_ID = "cardId";
    public static final String BUNDLE_EXTRAS_AMOUNT = "amount";
    public static final String BUNDLE_EXTRAS_RECEIPT_NO = "receiptNo";
    public static final String BUNDLE_EXTRAS_RECEIPT_Name = "receiptName";

    private RedeemCardConfirmationPresenter mRedeemCardConfirmationPresenter;

    public static RedeemCardConfirmationFragment newInstance(Integer cardId,String purchasecardid, String amount, String receiptNo, String receiptName) {
        RedeemCardConfirmationFragment redeemCardConfirmationFragment = new RedeemCardConfirmationFragment();
        Bundle bundle = new Bundle();

        bundle.putInt(BUNDLE_EXTRAS_CARD_ID, cardId);
        bundle.putString(BUNDLE_EXTRAS_AMOUNT, amount);
        bundle.putString(BUNDLE_EXTRAS_RECEIPT_NO, receiptNo);
        bundle.putString(BUNDLE_EXTRAS_RECEIPT_Name, receiptName);
        bundle.putString("purchasecardid", purchasecardid);
        redeemCardConfirmationFragment.setArguments(bundle);
        return redeemCardConfirmationFragment;

    }

    @Override public int getLayoutId() {
        return R.layout.fragment_redeem_payment_confirm;
    }

    @Override public void init() {
        //set toolbar title
        tvTitle.setText(R.string.fragment_title_redeem_card_confirmation);

        //initialize and attach presenter
        mRedeemCardConfirmationPresenter = new RedeemCardConfirmationPresenter();
        mRedeemCardConfirmationPresenter.attachView(this);

        //set total amount and partial amount to be deducted
        tvTotalAmount.setText(null != getArguments() ? "$"+getArguments().getString(BUNDLE_EXTRAS_AMOUNT) : "0");
        etAmount.setText(null != getArguments() ? "$"+getArguments().getString(BUNDLE_EXTRAS_AMOUNT) : "0");
        etAmount.addTextChangedListener(this);
    }

    @Override public BasePresenter getPresenter() {
        return null;
    }

    @OnClick({R.id.tvPartialPaymentText, R.id.btnDeduct})
    public void onClicked(View view) {
        switch (view.getId()) {
            case R.id.tvPartialPaymentText:
                //tvPartialnameText.setVisibility(View.VISIBLE);
                //etName.setVisibility(View.VISIBLE);

                etAmount.setVisibility(View.VISIBLE);
                tvPartialAmountText.setVisibility(View.VISIBLE);
                break;

            case R.id.btnDeduct:

                final String totalamount=RemoveDoller(tvTotalAmount.getText().toString());
                final String amount=RemoveDoller(etAmount.getText().toString());

                if (Double.parseDouble(amount)>Double.parseDouble(totalamount)){
                    Toast.makeText(getActivity(), "Please enter valid price", Toast.LENGTH_SHORT).show();
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(String.format("You are about to deduct this "+ etAmount.getText().toString().trim()+" amount from your eGift Card?", etAmount.getText().toString().trim()))
                            .setCancelable(true)
                            .setPositiveButton("Yes, Accept", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    mRedeemCardConfirmationPresenter.redeemCard(getArguments().getInt(BUNDLE_EXTRAS_CARD_ID),getArguments().getString("purchasecardid"), getArguments().getString(BUNDLE_EXTRAS_RECEIPT_NO), RedeemCardFragment.str_amount,amount,getArguments().getString(BUNDLE_EXTRAS_RECEIPT_Name));
                                }
                            })
                            .setNegativeButton("No, Don't Accept", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }

           /*     Log.e("total amount is",totalamount);
                Log.e("amount is",amount);*/


               /* String repl = etAmount.getText().toString().trim().replaceAll("(?<=\\d),(?=\\d)|\\$", "");
                Log.e("relace amount",repl);
*/

                break;
        }
    }

    @Override
    public void sendBroadcast() {
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
                new Intent(MyCardsFragment.INTENT_FILTER_UPDATE_MY_CARD_LIST));

        //openMyCardsFragment
        getFragmentManager().popBackStackImmediate();
        getFragmentManager().popBackStackImmediate();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
      /*  if(!s.toString().startsWith("$")){
            etAmount.setText("$");
            Selection.setSelection(etAmount.getText(), etAmount.getText().length());

        }*/

        if(!s.toString().equals(current)){

            etAmount.removeTextChangedListener(this);

            String cleanString = s.toString().replaceAll("[$,.]", "");

            double parsed = Double.parseDouble(cleanString);
            String formatted = NumberFormat.getCurrencyInstance(Locale.US).format((parsed/100));

            current = formatted;
            etAmount.setText(formatted);
            etAmount.setSelection(formatted.length());

            etAmount.addTextChangedListener(this);
        }

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public String RemoveDoller(String string){
        String repl = string.replaceAll("(?<=\\d),(?=\\d)|\\$", "");
        return repl;
    }
}
