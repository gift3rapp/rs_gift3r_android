package com.richestsoft.gift3r.views.interfaces;

import android.view.View;

/**
 * Created by Mukesh on 14/12/2016.
 */

public interface DialogFragmentCallbacks {

    void onDialogFragmentClickEvent(View view);
}
