package com.richestsoft.gift3r.views.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.TextView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.networkrequest.WebConstants;
import com.richestsoft.gift3r.models.pojos.UserProfile;
import com.richestsoft.gift3r.presenters.BasePresenter;
import com.richestsoft.gift3r.presenters.SettingsPresenter;
import com.richestsoft.gift3r.views.SettingsView;
import com.richestsoft.gift3r.views.dialogfragments.ChangePasswordDialogFragment;
import com.richestsoft.gift3r.views.dialogfragments.WebViewDialogFragment;
import com.richestsoft.gift3r.views.interfaces.ChangePasswordCallBacks;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by shray on 09-02-2018.
 */

public class SettingsFragment extends BaseFragment implements ChangePasswordCallBacks, SettingsView {

    @BindView(R.id.sdvUserImage) SimpleDraweeView sdvUserImage;
    @BindView(R.id.tvUserName) TextView tvUserName;
    @BindView(R.id.tvPhoneNo) TextView tvPhoneNumber;
    @BindView(R.id.tvEmail) TextView tvEmail;

    private SettingsPresenter mSettingsPresenter;

    @Override public int getLayoutId() {
        return R.layout.fragment_settings;
    }

    @Override public void init() {
        //initialize and attach presenter
        mSettingsPresenter = new SettingsPresenter();
        mSettingsPresenter.attachView(this);

        //set user profile text
        mSettingsPresenter.setUserProfile();
    }

    @Override public BasePresenter getPresenter() {
        return mSettingsPresenter;
    }

    @OnClick({R.id.tvChangePassword, R.id.tvPrivacyPolicy, R.id.tvTermsConditions, R.id.tvAboutUs, R.id.tvLogout, R.id.tvTermsOfUse})
    public void onClicked(View view) {
        switch (view.getId()) {
            case R.id.tvChangePassword:
                ChangePasswordDialogFragment mChangePasswordDialogFragment =
                        new ChangePasswordDialogFragment();
                mChangePasswordDialogFragment.setTargetFragment(this, 122);
                mChangePasswordDialogFragment.show(getFragmentManager(), getString(R.string.dialog));
                break;
            case R.id.tvPrivacyPolicy:
                WebViewDialogFragment.newInstance(getString(R.string.policy_privacy),
                        WebConstants.ACTION_PRIVACY_POLICY).show(getFragmentManager(), getString(R.string.dialog));
                break;
            case R.id.tvTermsConditions:
                WebViewDialogFragment.newInstance(getString(R.string.terms_conditions),
                        WebConstants.ACTION_TERMS_AND_CONDITIONS).show(getFragmentManager(), getString(R.string.dialog));
                break;
            case R.id.tvTermsOfUse:
                WebViewDialogFragment.newInstance(getString(R.string.terms_of_use),
                        WebConstants.ACTION_TERMS_OF_USE).show(getFragmentManager(), getString(R.string.dialog));
                break;
            case R.id.tvAboutUs:
                WebViewDialogFragment.newInstance(getString(R.string.about_us),
                        WebConstants.ACTION_ABOUT_US).show(getFragmentManager(), getString(R.string.dialog));
                break;
            case R.id.tvLogout:
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.logout_warning_msg)
                        .setCancelable(true)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mSettingsPresenter.logout();
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                break;
        }
    }

    // change password dialogFragment callback (ChangePasswordCallBacks)
    @Override public void onChangePasswordButtonClick(String oldPassword, String newPassword) {
        mSettingsPresenter.changePassword(oldPassword, newPassword);
    }

    @Override public void setUserProfile(UserProfile userProfile) {
        tvUserName.setText(userProfile.getName());
        tvEmail.setText(userProfile.getEmail());
        tvPhoneNumber.setText(userProfile.getPhone_number());
    }
}
