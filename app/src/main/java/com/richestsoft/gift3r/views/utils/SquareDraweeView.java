package com.richestsoft.gift3r.views.utils;

import android.content.Context;
import android.util.AttributeSet;

import com.facebook.drawee.view.SimpleDraweeView;

/**
 * Created by Mukesh on 05-05-2016.
 */
public class SquareDraweeView extends SimpleDraweeView {

    public SquareDraweeView(final Context context) {
        super(context);
    }

    public SquareDraweeView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareDraweeView(final Context context, final AttributeSet attrs,
                            final int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec,
                             final int heightMeasureSpec) {
        final int width = getDefaultSize(getSuggestedMinimumWidth(),
                widthMeasureSpec);
        setMeasuredDimension(width, width);
    }

    @Override
    protected void onSizeChanged(final int w, final int h, final int oldw,
                                 final int oldh) {
        super.onSizeChanged(w, w, oldw, oldh);
    }
}