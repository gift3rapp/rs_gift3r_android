package com.richestsoft.gift3r.views.utils.itemdecorations;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Mukesh on 01/06/2016.
 */
public class EqualSpacingDecorator extends RecyclerView.ItemDecoration {

    private int mItemOffset;

    public EqualSpacingDecorator(int itemOffset) {
        mItemOffset = itemOffset;
    }

    public EqualSpacingDecorator(@NonNull Context context, @DimenRes int itemOffsetId) {
        this(context.getResources().getDimensionPixelSize(itemOffsetId));
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
    }
}