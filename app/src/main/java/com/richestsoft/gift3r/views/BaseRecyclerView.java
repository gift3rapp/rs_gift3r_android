package com.richestsoft.gift3r.views;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mukesh on 23/03/2017.
 */

public interface BaseRecyclerView extends BaseView {

    void showSwipeRefreshLoader();

    void hideSwipeRefreshLoader();

    void showNoDataText(int resId, String message);

    void hideNoDataText();

    RecyclerView.Adapter getRecyclerViewAdapter();

    void updateRecyclerViewData(ArrayList<?> dataList);

}
