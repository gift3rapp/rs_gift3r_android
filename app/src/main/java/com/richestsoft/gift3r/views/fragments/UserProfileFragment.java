package com.richestsoft.gift3r.views.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.networkrequest.RestClient;
import com.richestsoft.gift3r.models.pojos.Favouritelist;
import com.richestsoft.gift3r.models.pojos.Get_User_Detail;
import com.richestsoft.gift3r.models.pojos.MyFavourites_Response;
import com.richestsoft.gift3r.models.pojos.Store;
import com.richestsoft.gift3r.utils.ApplicationGlobal;
import com.richestsoft.gift3r.views.activities.HomeActivity;
import com.richestsoft.gift3r.views.adapters.All_Contacts_Adapter;
import com.richestsoft.gift3r.views.adapters.MyRegistry_Adapter;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;


public class UserProfileFragment extends Fragment {

    View view;
    ImageView iv_back;
    ProgressBar pb_userdetail;
    LinearLayoutManager manager;
    TextView tv_username,tv_address;
    MyRegistry_Adapter myRegistry_adapter;
    public static String phonenumber;
    List<Store> stores=new ArrayList<>();
    RecyclerView rv_userfavorite;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_user_profile, container, false);
        Init();
        return view;
    }

    public void Init(){


        rv_userfavorite=view.findViewById(R.id.rv_userfavorite);
        pb_userdetail=view.findViewById(R.id.pb_userdetail);
        tv_address=view.findViewById(R.id.tv_address);
        tv_username=view.findViewById(R.id.tv_username);
        iv_back=view.findViewById(R.id.iv_back);
        manager=new GridLayoutManager(getActivity(), 2);
        rv_userfavorite.setLayoutManager(manager);


        GetProfileDetail();


        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }

    private void GetProfileDetail() {
        //    ivFavourite.setEnabled(false);
        //     myCustomLoader.showProgressDialog();
        RestClient.get().User_ProfieDetail(ApplicationGlobal.getSessionId(), All_Contacts_Adapter.Selectednumber)
                .enqueue(new Callback<Get_User_Detail>() {
                    @Override
                    public void onResponse(Call<Get_User_Detail> call,
                                           retrofit2.Response<Get_User_Detail> response) {
                        try {
                            //   Toast.makeText(mFragment.getActivity(), String.valueOf(response.body().getStatus()), Toast.LENGTH_SHORT).show();
                            pb_userdetail.setVisibility(View.GONE);
                            stores=response.body().getFavouritelist();
                            phonenumber=response.body().getProfile().getPhoneNumber();
                            tv_username.setText(response.body().getProfile().getName());
                            tv_address.setText(response.body().getProfile().getPhoneNumber());
                            myRegistry_adapter=new MyRegistry_Adapter(getActivity(), stores,"userprofile");
                            rv_userfavorite.setAdapter(myRegistry_adapter);
                        } catch (Exception e) {
                            //           myCustomLoader.hidedialog();
                            e.printStackTrace();

                            //  myCustomLoader.showToast(mContext.getString(R.string.error_general));
                        }
                    }

                    @Override
                    public void onFailure(Call<Get_User_Detail> call, Throwable t) {
                        //   ivFavourite.setEnabled(true);
                        //    myCustomLoader.hidedialog();
                        //      myCustomLoader.handleRetrofitError(mFragment.getView(), String.valueOf(t));
                    }
                });
    }

}
