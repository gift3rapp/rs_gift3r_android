package com.richestsoft.gift3r.views.fragments;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.presenters.BasePresenter;
import com.richestsoft.gift3r.presenters.SignInPresenter;
import com.richestsoft.gift3r.utils.GeneralFunctions;
import com.richestsoft.gift3r.views.SignInView;
import com.richestsoft.gift3r.views.activities.HomeActivity;
import com.richestsoft.gift3r.views.dialogfragments.ForgotPasswordDialogFragment;
import com.richestsoft.gift3r.views.interfaces.ForgotPasswordCallBacks;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by user28 on 6/2/18.
 */

public class SignInFragment extends BaseFragment implements ForgotPasswordCallBacks, SignInView {


    @BindView(R.id.etEmail) EditText etEmail;
    @BindView(R.id.etPassword) EditText etPassword;
    @BindView(R.id.tvSignUp) TextView tvSignUp;

    private SignInPresenter mSignInPresenter;

    @Override public int getLayoutId() {
        return R.layout.fragment_sign_in;
    }

    @Override public void init() {
        //initialize and attach presenter
        mSignInPresenter = new SignInPresenter();
        mSignInPresenter.attachView(this);

        // creating spannable string Don't have an account?
        SpannableString spannableString = new SpannableString(getString(R.string.sign_up_text));

        String signup = getString(R.string.sign_up);
        int signupStartSpan = spannableString.toString().indexOf(signup);
        int signupEndSpan = signupStartSpan + signup.length();

        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                GeneralFunctions.replaceFragmentWithBackStack(getFragmentManager(),
                        R.animator.slide_right_in, 0, 0,
                        R.animator.slide_right_out, new SignUpFragment(), null,
                        R.id.clFragContainerMain);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(ContextCompat.getColor(getActivityContext(), R.color.colorAccent));
            }
        }, signupStartSpan, signupEndSpan, 0);

        tvSignUp.setText(spannableString);
        tvSignUp.setMovementMethod(new LinkMovementMethod());
    }

    @Override public BasePresenter getPresenter() {
        return mSignInPresenter;
    }

    @OnClick({R.id.btnSignIn, R.id.tvForgotPwd})
    public void onClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSignIn:

         //       if (ApplicationGlobal.isNetworkAvailable(getActivityContext())){
                    mSignInPresenter.signInUser(etEmail.getText().toString().trim(),
                            etPassword.getText().toString().trim());
             /*   }else {
                    Toast.makeText(getActivityContext(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }*/

                break;
            case R.id.tvForgotPwd:
                ForgotPasswordDialogFragment forgotPasswordDialogFragment =
                        new ForgotPasswordDialogFragment();
                forgotPasswordDialogFragment.setTargetFragment(this, 122);
                forgotPasswordDialogFragment.show(getFragmentManager(), getString(R.string.dialog));
                break;
        }
    }

    // forgot password dialogFragment callback (ForgotPasswordCallBacks)
    @Override public void onForgotPasswordSubmit(String email) {
        mSignInPresenter.forgotPassword(email);
    }

    //presenter callbacks (SignInView)
    @Override public void navigateToHomeScreen() {
        startActivity(new Intent(getActivity(), HomeActivity.class));
        getActivity().finish();
    }
}
