package com.richestsoft.gift3r.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.preferences.UserPrefsManager;
import com.richestsoft.gift3r.presenters.BasePresenter;
import com.richestsoft.gift3r.utils.GeneralFunctions;
import com.richestsoft.gift3r.utils.MyCustomLoader;

import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Mukesh on 01/09/2016.
 */
public abstract class BaseAppCompatActivity extends AppCompatActivity {

    private MyCustomLoader mMyCustomLoader;
    private UserPrefsManager mUserPrefsManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        if (GeneralFunctions.isAboveLollipopDevice()) {
            Window window = getWindow();
            if (isMakeStatusBarTransparent()) {
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorTransparent));
                window.getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            } else {
                window.setStatusBarColor(ContextCompat.getColor(this,
                        R.color.colorPrimaryDark));
            }
        }
//        if (getUserPrefsManager().getIsLogined()) {
//            GeneralFunctions.setSessionId(getUserPrefsManager().getSessionId());
//        }
        init();
    }

    @Override
    public void onBackPressed() {
        if (null != getFragmentManager() && 0 < getFragmentManager().getBackStackEntryCount()) {
            getFragmentManager().popBackStackImmediate();
        } else {
            finish();
            super.onBackPressed();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public MyCustomLoader getMyCustomLoader() {
        if (null == mMyCustomLoader) {
            mMyCustomLoader = new MyCustomLoader(this);
        }
        return mMyCustomLoader;
    }

    public UserPrefsManager getUserPrefsManager() {
        if (null == mUserPrefsManager) {
            mUserPrefsManager = new UserPrefsManager(this);
        }
        return mUserPrefsManager;
    }

    public Context getActivityContext() {
        return this;
    }

    public boolean isFragmentDestroyed() {
        return false;
    }

    public void showProgressLoader() {
        if (null != getMyCustomLoader()) {
            getMyCustomLoader().showProgressDialog(getString(R.string.loading));
        }
    }

    public void hideProgressLoader() {
        if (null != getMyCustomLoader()) {
            getMyCustomLoader().dismissProgressDialog();
        }
    }

    public void showMessage(int resId, String message, boolean isShowSnackbarMessage) {
        if (null != getMyCustomLoader()) {
            if (isShowSnackbarMessage) {
                getMyCustomLoader().showSnackBar(this.findViewById(android.R.id.content),
                        null != message ? message : getString(resId));
            } else {
                getMyCustomLoader().showToast(null != message ? message : getString(resId));
            }
        }
    }

    public void expireUserSession() {
        hideProgressLoader();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    protected void onDestroy() {
        if (null != getPresenter()) {
            getPresenter().detachView();
        }
        super.onDestroy();
    }


    public abstract boolean isMakeStatusBarTransparent();

    public abstract int getLayoutId();

    public abstract void init();

    public abstract BasePresenter getPresenter();

}
