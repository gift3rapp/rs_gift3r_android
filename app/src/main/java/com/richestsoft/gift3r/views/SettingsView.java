package com.richestsoft.gift3r.views;

import com.richestsoft.gift3r.models.pojos.UserProfile;

/**
 * Created by shray on 09-02-2018.
 */

public interface SettingsView extends BaseView {

    void setUserProfile(UserProfile userPRofile);
}
