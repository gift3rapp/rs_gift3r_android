package com.richestsoft.gift3r.views.dialogfragments;

import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.utils.GeneralFunctions;
import com.richestsoft.gift3r.views.interfaces.ChangePasswordCallBacks;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Mukesh on 12/10/2016.
 */
public class ChangePasswordDialogFragment extends BaseDialogFragment {

    @BindView(R.id.etOldPassword) EditText etOldPassword;
    @BindView(R.id.etNewPassword) EditText etNewPassword;
    @BindView(R.id.etConfirmPassword) EditText etConfirmPassword;

    @Override
    public boolean getIsFullScreenDialog() {
        return false;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_fragment_change_password;
    }

    @Override
    public void init() {
        // set title
        tvTitle.setText(getString(R.string.title_change_password));
    }

    @OnClick(R.id.btnChangePassword)
    public void changePassword() {
        if (null != getTargetFragment()) {

            if (TextUtils.isEmpty(etOldPassword.getText().toString())){
                Toast.makeText(getActivity(), "Please enter old password", Toast.LENGTH_SHORT).show();
            }else if (!GeneralFunctions.isValidPassword(etOldPassword.getText().toString())){
                Toast.makeText(getActivity(), getResources().getString
                        (R.string.invalid_password), Toast.LENGTH_SHORT).show();
            }
            else if (TextUtils.isEmpty(etNewPassword.getText().toString())){
                Toast.makeText(getActivity(), "Please enter new password", Toast.LENGTH_SHORT).show();
            }else if (!GeneralFunctions.isValidPassword(etNewPassword.getText().toString())){
                Toast.makeText(getActivity(), getResources().getString
                        (R.string.invalid_password), Toast.LENGTH_SHORT).show();
            }else if (TextUtils.isEmpty(etConfirmPassword.getText().toString())){
                Toast.makeText(getActivity(), "Please enter confirm password", Toast.LENGTH_SHORT).show();
            }else if (!GeneralFunctions.isValidPassword(etConfirmPassword.getText().toString())){
                Toast.makeText(getActivity(), getResources().getString
                        (R.string.invalid_password), Toast.LENGTH_SHORT).show();
            }else {
                if (etNewPassword.getText().toString().trim().equals(etConfirmPassword.
                        getText().toString().trim())) {
                    ((ChangePasswordCallBacks) getTargetFragment())
                            .onChangePasswordButtonClick(etOldPassword.getText().toString().trim(),
                                    etNewPassword.getText().toString().trim());
                } else {
                    Toast.makeText(getActivity(), getResources().getString
                            (R.string.confirm_password_do_not_match), Toast.LENGTH_SHORT).show();
                }

            }

        }
    }
}
