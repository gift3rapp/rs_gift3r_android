package com.richestsoft.gift3r.views.adapters;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.pojos.Favouritelist;
import com.richestsoft.gift3r.models.pojos.Store;
import com.richestsoft.gift3r.utils.GeneralFunctions;
import com.richestsoft.gift3r.utils.ScreenDimensions;
import com.richestsoft.gift3r.views.fragments.StoreDetailFragment;
import com.richestsoft.gift3r.views.fragments.UserProfileFragment;

import java.util.ArrayList;
import java.util.List;

public class MyRegistry_Adapter extends RecyclerView.Adapter<MyRegistry_Adapter.MyViewHolder> {

    Context context;
    private int imageWidth, imageHeight;
    String type;
    List<Store> favouritelists=new ArrayList<>();

    public MyRegistry_Adapter(Context context, List<Store> favouritelists,String type) {

        this.context = context;
        this.type = type;
        this.favouritelists = favouritelists;
        imageWidth = new ScreenDimensions(context).getScreenWidth();
        imageHeight = (int)context.getResources().getDimension(R.dimen.store_Detail_image_height);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_store, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, final int position) {

        try {

            myViewHolder.tvStoreName.setText(favouritelists.get(position).getName());
            myViewHolder.simpleDraweeView.setImageURI(favouritelists.get(position).getImage());

            myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager manager = ((Activity) context).getFragmentManager();

                    if (type.equalsIgnoreCase("registry")){
                        GeneralFunctions.addFragment(manager, R.animator.slide_right_in, 0, 0, R.animator.slide_right_out, StoreDetailFragment.newInstance(favouritelists.get(position),"2"), null, R.id.flFragmentContainerHome);
                    }else {
                        GeneralFunctions.addFragment(manager, R.animator.slide_right_in, 0, 0, R.animator.slide_right_out, StoreDetailFragment.newInstance(favouritelists.get(position),"3", UserProfileFragment.phonenumber), null, R.id.flFragmentContainerHome);
                    }
                //    GeneralFunctions.addFragment(manager, R.animator.slide_right_in, 0, 0, R.animator.slide_right_out, StoreDetailFragment.newInstance(favouritelists.get(position),"2"), null, R.id.flFragmentContainerHome);


                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (favouritelists==null){
            return 0;
        }else {
            return favouritelists.size();
        }


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        SimpleDraweeView simpleDraweeView;
        TextView tvStoreName;
        public MyViewHolder(View view) {
            super(view);

            simpleDraweeView=view.findViewById(R.id.sdvStoreImage);
            tvStoreName=view.findViewById(R.id.tvStoreName);

        }
    }


}