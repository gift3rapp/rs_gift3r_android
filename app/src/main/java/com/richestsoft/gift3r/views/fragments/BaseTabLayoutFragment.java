package com.richestsoft.gift3r.views.fragments;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.pojos.Tab;
import com.richestsoft.gift3r.utils.GeneralFunctions;
import com.richestsoft.gift3r.views.adapters.TabsAdapter;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Mukesh on 12/08/2016.
 */
public abstract class BaseTabLayoutFragment extends BaseFragment {

    @BindView(R.id.tabLayout) TabLayout tabLayout;
    @BindView(R.id.viewPager) ViewPager viewPager;

    @Override
    public void init() {
        if (null != toolbar) {
            // remove toolbar background and elevation
            if (GeneralFunctions.isAboveLollipopDevice()) {
                toolbar.setElevation(0);
            }
            toolbar.setBackgroundColor(Color.TRANSPARENT);
        }
        initTabs();
    }

    public void setViewPager(String toolbarTitle, List<Tab> tabsList) {
        if (null != tvTitle) {
            tvTitle.setText(toolbarTitle);
        }
        TabsAdapter tabsAdapter = new TabsAdapter(getChildFragmentManager(), tabsList);
        if (3 < tabsList.size()) {
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(tabsAdapter);
        tabLayout.setupWithViewPager(viewPager);
        changeTabsFont(tabLayout);

        // set icons for tabs
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            tabLayout.getTabAt(i).setIcon(tabsList.get(i).getTabIcon());
        }
    }

    // change tab title font
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void changeTabsFont(TabLayout tabLayout) {
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild)
                            .setTypeface(Typeface.createFromAsset(getActivity().getAssets(),
                                    getActivity().getString(R.string.font_semibold)));
                }
            }
        }
    }

    public abstract void initTabs();
}
