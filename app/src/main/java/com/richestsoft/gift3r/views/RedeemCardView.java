package com.richestsoft.gift3r.views;

/**
 * Created by user28 on 13/2/18.
 */

public interface RedeemCardView extends BaseView {

    void proceedToConfirmation(Integer cardId, String purchasecardid,String amount, String receiptNo,String receiptName);

}
