package com.richestsoft.gift3r.views.interfaces;

/**
 * Created by user28 on 13/11/17.
 */

public interface LoadMoreListener {
    void onLoadMore();
}
