package com.richestsoft.gift3r.views.dialogfragments;

import android.widget.EditText;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.views.interfaces.ForgotPasswordCallBacks;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Mukesh on 12/10/2016.
 */
public class ForgotPasswordDialogFragment extends BaseDialogFragment {

    @BindView(R.id.etEmail) EditText etEmail;

    @Override
    public boolean getIsFullScreenDialog() {
        return false;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_fragment_forgot_password;
    }

    @Override
    public void init() {
        // set title
        tvTitle.setText(getString(R.string.title_forgot_password));
    }

    @OnClick(R.id.btnSubmit)
    public void forgotPassword() {
        if (null != getTargetFragment()) {
            ((ForgotPasswordCallBacks) getTargetFragment())
                    .onForgotPasswordSubmit(etEmail.getText().toString().trim());
        }
    }
}
