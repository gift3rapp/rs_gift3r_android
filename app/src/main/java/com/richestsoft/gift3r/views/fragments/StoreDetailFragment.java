package com.richestsoft.gift3r.views.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.networkrequest.RestClient;
import com.richestsoft.gift3r.models.pojos.Favouritelist;
import com.richestsoft.gift3r.models.pojos.Payment_Request;
import com.richestsoft.gift3r.models.pojos.Payment_Response;
import com.richestsoft.gift3r.models.pojos.Store;
import com.richestsoft.gift3r.presenters.BasePresenter;
import com.richestsoft.gift3r.presenters.MyCardsPresenter;
import com.richestsoft.gift3r.presenters.StoreDetailPresenter;
import com.richestsoft.gift3r.utils.ApplicationGlobal;
import com.richestsoft.gift3r.views.StoreDetailView;
import com.richestsoft.gift3r.views.activities.HomeActivity;
import com.richestsoft.gift3r.views.adapters.StoreDetailAdapter;
import com.richestsoft.gift3r.views.interfaces.BuyGiftCardCallBacks;

import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;

/**
 * Created by user28 on 9/2/18.
 */

public class StoreDetailFragment extends BaseRecyclerViewFragment implements StoreDetailView, BuyGiftCardCallBacks {

    public static final String BUNDLE_EXTRAS_STORE = "store";
    public final int RESULT_PICK_CONTACT = 21;
    private Integer selectedCardId;
    private String purchasecardid;
    private String message;
    public static String searchid;
    private StoreDetailPresenter mStoreDetailPresenter;
    private StoreDetailAdapter mStoreDetailAdapter;
    private Store mStore;
    private Favouritelist favouritelist;
    ArrayList<String> list_of_month = new ArrayList<>();
    ArrayList<String> list_of_year = new ArrayList<>();
    public int mm_position;
    public int yy_position;
    public static Activity activity;
    private String selected_mm;
    private String selected_yy;
    String selectedSuperStar;
    public static String phonenumber;
    public static RelativeLayout rl_prog;
    int current_year, max_year;
    int month;
    private MyCardsPresenter mMyCardsPresenter;
    public static View view;
    int current_month;

    public static StoreDetailFragment newInstance(Store store, String searchid) {
        StoreDetailFragment storeDetailFragment = new StoreDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("id",searchid);
        bundle.putParcelable(BUNDLE_EXTRAS_STORE, store);
        storeDetailFragment.setArguments(bundle);
        return storeDetailFragment;
    }

    public static StoreDetailFragment newInstance(Store store, String searchid,String phonenumber) {
        StoreDetailFragment storeDetailFragment = new StoreDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("id",searchid);
        bundle.putString("phonenumber",phonenumber);
        bundle.putParcelable(BUNDLE_EXTRAS_STORE, store);
        storeDetailFragment.setArguments(bundle);
        return storeDetailFragment;
    }

   /* public static Fragment newInstance(Favouritelist favouritelist, String  searchid) {
        StoreDetailFragment storeDetailFragment = new StoreDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("id",searchid);
        bundle.putParcelable(BUNDLE_EXTRAS_STORE,favouritelist);
        storeDetailFragment.setArguments(bundle);
        return storeDetailFragment;
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         view=inflater.inflate(R.layout.fragment_store_detail, container, false);
         return view;
    }

    @Override public int getLayoutId() {
        return R.layout.fragment_store_detail;
    }

    @Override public void setData() {

        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        rl_prog=view.findViewById(R.id.rl_prog);
        activity=getActivity();
        mMyCardsPresenter = new MyCardsPresenter();
        // set toolbar title
        tvTitle.setText(null != mStore ? mStore.getName() : getString(R.string.store_detail_default_title));

        searchid=getArguments().getString("id");

        if (searchid.equalsIgnoreCase("3")){
            phonenumber=getArguments().getString("phonenumber");
        }else {

        }

        if (null != toolbar) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Open_HomeActivity();

                   /* getFragmentManager().popBackStackImmediate();
                    HomeActivity.viewPager1.getAdapter().notifyDataSetChanged();*/

                }
            });
        }

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                   /* startActivity(new Intent(getActivity(), HomeActivity.class));
                    getActivity().finish();*/
                    Open_HomeActivity();

                    return true;
                }
                return false;
            }
        });

        //initialize and attach presenter
        mStoreDetailPresenter = new StoreDetailPresenter();
        mStoreDetailPresenter.attachView(this);
    }

    @Override public RecyclerView.LayoutManager getLayoutManager() {
        return null;
    }

    @Override public RecyclerView.Adapter getRecyclerViewAdapter() {

        // get store data to display

        mStore = null != getArguments() ? (Store) getArguments().getParcelable(BUNDLE_EXTRAS_STORE) : new Store();
        if (null == mStoreDetailAdapter) {
            mStoreDetailAdapter = new StoreDetailAdapter(this, mStore,favouritelist);
        }
        return mStoreDetailAdapter;

      /*  if (getArguments().getString("id").equals("1")){
            mStore = null != getArguments() ? (Store) getArguments().getParcelable(BUNDLE_EXTRAS_STORE) : new Store();
            if (null == mStoreDetailAdapter) {
                mStoreDetailAdapter = new StoreDetailAdapter(this, mStore,favouritelist);
            }
            return mStoreDetailAdapter;
        }else {
            favouritelist = null != getArguments() ? (Favouritelist) getArguments().getParcelable(BUNDLE_EXTRAS_STORE) : new Favouritelist();
            if (null == mStoreDetailAdapter) {
                mStoreDetailAdapter = new StoreDetailAdapter(this,mStore, favouritelist);
            }
            return mStoreDetailAdapter;
        }*/
    }

    @Override public void updateRecyclerViewData(ArrayList<?> dataList) {

    }

    @Override public boolean isShowRecyclerViewDivider() {
        return false;
    }

    @Override public void onPullDownToRefresh() {
    }

    @Override public BasePresenter getPresenter() {
        return mStoreDetailPresenter;
    }

    @Override public void sendGiftCard(Integer cardId,String mypurchasecardid,String  message) {
        selectedCardId = cardId;
        purchasecardid = mypurchasecardid;
        this.message = message;
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, RESULT_PICK_CONTACT);
    }


    public void Open_HomeActivity(){

        if (searchid.equalsIgnoreCase("1")){
            Intent intent=new Intent(getActivity(),HomeActivity.class);
            intent.putExtra("type","stores");
            startActivity(intent);
            getActivity().finish();
        } else if (searchid.equalsIgnoreCase("2")) {
            Intent intent=new Intent(getActivity(),HomeActivity.class);
            intent.putExtra("type","registry");
            startActivity(intent);
            getActivity().finish();
        }else if (searchid.equalsIgnoreCase("3")) {
            Intent intent=new Intent(getActivity(),HomeActivity.class);
            intent.putExtra("type","contact");
            startActivity(intent);
            getActivity().finish();
        }else {

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_PICK_CONTACT && resultCode == RESULT_OK) {
            Uri contactUri = data.getData();
            final Cursor cursor = getActivity().getContentResolver().query(contactUri, null, null, null, null);
            cursor.moveToFirst();
           // final int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            final String column = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String Name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

          /*  String selectednumber = null;
            String id =cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
            String hasPhone =cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

            if (hasPhone.equalsIgnoreCase("1")) {
                Cursor phones = getActivity().getContentResolver().query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,
                        null, null);
                phones.moveToFirst();
                selectednumber = phones.getString(phones.getColumnIndex("data1"));
                System.out.println("number is:"+selectednumber);
            }
*/
       //     Log.e("phone number is",column);

            //show warning before sending card
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("You are about to purchase this card and sent to "+Name)
                    .setCancelable(true)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            StoreDetailAdapter.yes=true;

                            WriteNotePopUp(String.valueOf(column));
                            cursor.close();
                        /*    if (StoreDetailAdapter.buy){
                                WriteNotePopUp(String.valueOf(column));
                            //    mStoreDetailPresenter.buyMyCard(selectedCardId, cursor.getString(column),message);
                                cursor.close();
                            }else {

                            }*/

                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            cursor.close();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    @Override
    public void buyGiftCard(final Integer cardId, final String phoneNumber,final String message,final int payment_id) {

        mStoreDetailPresenter.buyMyCard(cardId, phoneNumber,message,payment_id);

    /*    //show warning before purchasing card
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.card_purchase_warning_msg)
                .setCancelable(true)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mStoreDetailPresenter.buyMyCard(cardId, phoneNumber,message);
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();*/
    }

    @Override
    public void sendBroadcast() {
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
                new Intent(MyCardsFragment.INTENT_FILTER_UPDATE_MY_CARD_LIST));
    }



    private void WriteNotePopUp(final String phonenumber) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.writenotepopup);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);
        // set the custom dialog components - text, image and button
        Button btn_sendnote = (Button) dialog.findViewById(R.id.btn_sendnote);
        final EditText et_writenotes = (EditText) dialog.findViewById(R.id.et_writenotes);
        final TextView tv_readnote = (TextView) dialog.findViewById(R.id.tv_readnote);

        btn_sendnote.setVisibility(View.VISIBLE);
        et_writenotes.setEnabled(true);
        et_writenotes.setFocusable(true);
        tv_readnote.setText("WRITE A NOTE");
        btn_sendnote.setText("Save");

        btn_sendnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(et_writenotes.getText().toString().trim())){
                    Toast.makeText(getActivity(), "Please enter personalized message", Toast.LENGTH_SHORT).show();
                }else {

                    Show_Card_Detail_Popup(StoreDetailAdapter.SelectedAmount,phonenumber,et_writenotes.getText().toString().trim(),StoreDetailAdapter.SelectedGiftcardid);

                    dialog.dismiss();
                }

            }
        });

        dialog.show();
    }

    public void Show_Card_Detail_Popup(final String amount,final String phonenumber, final String notes,final String SelectedGiftcardid) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.card_detail_layout);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        list_of_year.clear();
        list_of_month.clear();
        Calendar c = Calendar.getInstance();
        current_year = c.get(Calendar.YEAR)-1;
        month = c.get(Calendar.MONTH);

        current_month = 0;
        for (int i = 0; i < 13; i++) {
            max_year = current_year + i;

            // list_of_month.add(String.valueOf(current_month + i));
            if (i ==0) {

                list_of_month.add(i,"MM");
                list_of_year.add(i,"YY");

            }else {

                if (i <= 9) {
                    list_of_month.add(i, "0" + String.valueOf(current_month + i));

                }else {
                    list_of_month.add(i, String.valueOf(current_month + i));
                }

                String substring = null;
                try {
                    substring = String.valueOf(current_year).substring(Math.max(String.valueOf(current_year).length() - 2, 0));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    list_of_year.add(i,String.valueOf(Integer.parseInt(substring) + i));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

            }
        }

        final RadioButton rb_ae, rb_visa, rb_discover, rb_mastercard, sheamus;
        Button btn_pay = (Button) dialog.findViewById(R.id.btn_pay);
        ImageView card_info = dialog.findViewById(R.id.card_info);
        ImageView back = (ImageView) dialog.findViewById(R.id.back);
        RelativeLayout ll_visa = dialog.findViewById(R.id.ll_visa);
        RelativeLayout ll_mastercard = dialog.findViewById(R.id.ll_mastercard);
        RelativeLayout ll_discover = dialog.findViewById(R.id.ll_discover);
        RelativeLayout ll_ae = dialog.findViewById(R.id.ll_ae);
        final EditText et_cardholdername = dialog.findViewById(R.id.et_cardholdername);
        final EditText et_cardnumber = dialog.findViewById(R.id.et_cardnumber);
        final EditText et_month = dialog.findViewById(R.id.et_month);
        final EditText et_cardsecondname = dialog.findViewById(R.id.et_cardsecondname);
        final EditText et_year = dialog.findViewById(R.id.et_year);
        final EditText et_cvv = dialog.findViewById(R.id.et_cvv);
        final EditText et_zipcode = dialog.findViewById(R.id.et_zipcode);
        final TextView tv_amount = dialog.findViewById(R.id.tv_amount);
        final TextView tv_cardtype = dialog.findViewById(R.id.tv_cardtype);
        final RelativeLayout rl_cardprogress = dialog.findViewById(R.id.rl_cardprogress);
        Spinner spin_mm = (Spinner) dialog.findViewById(R.id.spn_mm);
        Spinner spin_yy = (Spinner) dialog.findViewById(R.id.spn_yy);
        //  radioGroup = (RadioGroup) dialog.findViewById(R.id.radiogroup);
        rb_ae = (RadioButton) dialog.findViewById(R.id.rb_ae);
        rb_visa = (RadioButton) dialog.findViewById(R.id.rb_visa);
        rb_discover = (RadioButton) dialog.findViewById(R.id.rb_discover);
        rb_mastercard = (RadioButton) dialog.findViewById(R.id.rb_mastercard);

        RadioGroup radioGroup;

        ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, list_of_month);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//Setting the ArrayAdapter data on the Spinner
        spin_mm.setAdapter(aa);
        selectedSuperStar = "Visa";
      /*  rb_visa.setChecked(false);
        rb_mastercard.setChecked(false);
        rb_discover.setChecked(false);
        rb_ae.setChecked(false);*/

/*
        if (rb_visa.isChecked()) {

        } else if (rb_ae.isChecked()) {

        } else if (rb_mastercard.isChecked()) {

        } else if (rb_discover.isChecked()) {

        }
*/

        ll_visa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rb_visa.setChecked(true);
                rb_mastercard.setChecked(false);
                rb_discover.setChecked(false);
                rb_ae.setChecked(false);

                selectedSuperStar = "Visa";
                et_cvv.setText("");
                InputFilter[] FilterArray = new InputFilter[1];
                FilterArray[0] = new InputFilter.LengthFilter(3);
                et_cvv.setFilters(FilterArray);
            }
        });

        ll_mastercard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_visa.setChecked(false);
                rb_mastercard.setChecked(true);
                rb_discover.setChecked(false);
                rb_ae.setChecked(false);
                selectedSuperStar = "MasterCard";
                et_cvv.setText("");
                InputFilter[] FilterArray = new InputFilter[1];
                FilterArray[0] = new InputFilter.LengthFilter(3);
                et_cvv.setFilters(FilterArray);
            }
        });


        ll_discover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rb_visa.setChecked(false);
                rb_mastercard.setChecked(false);
                rb_discover.setChecked(true);
                rb_ae.setChecked(false);
                selectedSuperStar = "Discover";
                et_cvv.setText("");
                InputFilter[] FilterArray = new InputFilter[1];
                FilterArray[0] = new InputFilter.LengthFilter(3);
                et_cvv.setFilters(FilterArray);

            }
        });

        ll_ae.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_visa.setChecked(false);
                rb_mastercard.setChecked(false);
                rb_discover.setChecked(false);
                rb_ae.setChecked(true);
                selectedSuperStar = "American Express";
                et_cvv.setText("");
                InputFilter[] FilterArray = new InputFilter[1];
                FilterArray[0] = new InputFilter.LengthFilter(4);
                et_cvv.setFilters(FilterArray);
            }
        });

        spin_mm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mm_position=position;

                // Object kkk = parent.getItemAtPosition(position);

                selected_mm=list_of_month.get(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter aaa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, list_of_year);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//Setting the ArrayAdapter data on the Spinner
        spin_yy.setAdapter(aaa);

        spin_yy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                yy_position=position;
                selected_yy=list_of_year.get(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // new DecimalFormat("#.##").format(mStore.getAvailable_cards().get(pos - 1).getPrice());

        // tv_amount.setText("$"+String.valueOf(mStore.getAvailable_cards().get(pos - 1).getPrice()));

        tv_amount.setText(String.format(getString(R.string.price_format2),Double.parseDouble(amount)));
        tv_cardtype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Select_Options(tv_cardtype);

            }
        });

        card_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //    Toast.makeText(mFragment.getActivity(), "kjhdjkfhskd", Toast.LENGTH_SHORT).show();

                new AlertDialog.Builder(getActivity())
                        .setTitle("About CVV")
                        .setMessage("3-digit security code usually found on the back of your card.American express cards have 4-digit code located on the front.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).show();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(et_cardholdername.getText().toString().trim())) {
                    Toast.makeText(getActivity(), "Please enter First name", Toast.LENGTH_SHORT).show();
                } else   if (TextUtils.isEmpty(et_cardsecondname.getText().toString().trim())) {
                    Toast.makeText(getActivity(), "Please enter Last name", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(et_zipcode.getText().toString().trim())) {
                    Toast.makeText(getActivity(), "Please enter zip code", Toast.LENGTH_SHORT).show();
                }else if (TextUtils.isEmpty(et_cardnumber.getText().toString().trim())) {
                    Toast.makeText(getActivity(), "Please enter card number", Toast.LENGTH_SHORT).show();
                } else if (et_cardnumber.getText().toString().trim().length()!=16) {
                    Toast.makeText(getActivity(), "Card not Valid, please enter the correct card numbers", Toast.LENGTH_SHORT).show();
                } else if (mm_position==0) {
                    Toast.makeText(getActivity(), "Please select month", Toast.LENGTH_SHORT).show();

               /* } else if (Integer.parseInt(et_month.getText().toString().trim()) < current_month || Integer.parseInt(et_month.getText().toString().trim()) > 12) {

                    Toast.makeText(mFragment.getActivity(), "Please enter valid month", Toast.LENGTH_SHORT).show();*/

                } else if (yy_position==0) {

                    Toast.makeText(getActivity(), "Please select year", Toast.LENGTH_SHORT).show();

                }/* else if (Integer.parseInt(selected_yy) <= 31) {

                    Toast.makeText(mFragment.getActivity(), "Please  select valid year", Toast.LENGTH_SHORT).show();

                }*/ else if (TextUtils.isEmpty(et_cvv.getText().toString().trim())) {
                    Toast.makeText(getActivity(), "Please enter cvv", Toast.LENGTH_SHORT).show();
                }else if (selectedSuperStar.equalsIgnoreCase("American Express") && et_cvv.getText().toString().length()<=3) {
                    Toast.makeText(getActivity(), "Please enter valid cvv", Toast.LENGTH_SHORT).show();
                } else if (et_cvv.getText().toString().length()<=2) {
                    Toast.makeText(getActivity(), "Please enter valid cvv", Toast.LENGTH_SHORT).show();
                }else {

                    Payment_Request payment_request = new Payment_Request();
                    payment_request.setAmount(amount);
                    payment_request.setCardHoldername(et_cardholdername.getText().toString().trim()+" "+et_cardsecondname.getText().toString().trim());
                    payment_request.setCardNumber(et_cardnumber.getText().toString().trim());
                    //payment_request.setCardType(tv_cardtype.getText().toString().trim());
                    payment_request.setCardType(selectedSuperStar);
                    payment_request.setCvv(et_cvv.getText().toString().trim());
                    payment_request.setExpiryDate(selected_mm+selected_yy);


/*
                 /*   String substring = selected_yy.substring(Math.max(selected_yy.length() - 2, 0));

                  if (Integer.parseInt(selected_mm) <= 9) {
                        payment_request.setExpiryDate(0 + selected_mm + substring);
                    } else {
                        payment_request.setExpiryDate(selected_mm + substring);
                    }*/

                    payment_request.setSessionId(ApplicationGlobal.getSessionId());
                    payment_request.setCardId(SelectedGiftcardid);
                    payment_request.setZipcode(et_zipcode.getText().toString().trim());

                    Log.d("zipcode",et_zipcode.getText().toString().trim());

                    payment_Api(payment_request, dialog, rl_cardprogress,phonenumber,notes);

                    Log.d("smlsdmvskl",payment_request.toString());



                }
            }
        });

        dialog.show();

    }

    private void Select_Options(final TextView textView) {

        final CharSequence[] items = {"VISA", "mastercard", "American Express"};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle("  Select Card!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                textView.setText(items[item]);
            }
        });
        builder.show();
    }

    private void payment_Api(Payment_Request payment_request, final Dialog dialog, final RelativeLayout rl_cardprogress, final String phonenumber, final String notes) {

        rl_cardprogress.setVisibility(View.VISIBLE);

        RestClient.get().Payment("application/json",payment_request)
                .enqueue(new Callback<Payment_Response>() {
                    @Override
                    public void onResponse(Call<Payment_Response> call, retrofit2.Response<Payment_Response> response) {
                        try {

                            rl_cardprogress.setVisibility(View.GONE);
                            dialog.dismiss();
                            if (response.body().getError() == false) {

                              //  buy = true;
/*                               if (type.equals("BUY")) {
                                    mBuyGiftCardCallBacks.buyGiftCard(mStore.getAvailable_cards().get(pos - 1).getGift_card_id(), "", "");
                                } else {
                                    mBuyGiftCardCallBacks.sendGiftCard(mStore.getAvailable_cards().get(pos - 1).getGift_card_id(), mStore.getAvailable_cards().get(pos - 1).getMypurchasedcard_id(), notes);
                                    //  WriteNotePopUp(pos, "FRIEND");
                                }*/

                                    Log.d("sssssssss",""+response.body().getPaymentId());
                                    mStoreDetailPresenter.buyMyCard(selectedCardId,phonenumber,notes,response.body().getPaymentId());

                             //   mMyCardsPresenter.sendMyCard(selectedCardId,purchasecardid,phonenumber,message);

                                Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            //           myCustomLoader.hidedialog();
                            e.printStackTrace();

                            //  myCustomLoader.showToast(mContext.getString(R.string.error_general));
                        }
                    }

                    @Override
                    public void onFailure(Call<Payment_Response> call, Throwable t) {
                       Toast.makeText(activity, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        Log.d("error",""+t.getMessage()+"==="+call);

                        //   ivFavourite.setEnabled(true);
                        //    myCustomLoader.hidedialog();
                        //      myCustomLoader.handleRetrofitError(mFragment.getView(), String.valueOf(t));
                    }
                });
    }

}