package com.richestsoft.gift3r.presenters;

import android.text.TextUtils;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.interactors.SignInInteractor;
import com.richestsoft.gift3r.models.networkrequest.NetworkRequestCallbacks;
import com.richestsoft.gift3r.models.networkrequest.RetrofitRequest;
import com.richestsoft.gift3r.models.pojos.PojoCommon;
import com.richestsoft.gift3r.models.pojos.PojoNetworkResponse;
import com.richestsoft.gift3r.models.pojos.PojoUserSignIn;
import com.richestsoft.gift3r.utils.GeneralFunctions;
import com.richestsoft.gift3r.views.SignInView;
import retrofit2.Response;

/**
 * Created by user28 on 13/2/18.
 */

public class SignInPresenter extends BasePresenter<SignInView> {

    private SignInInteractor mSignInInteractor;

    @Override public void attachView(SignInView view) {
        super.attachView(view);
        mSignInInteractor = new SignInInteractor();
    }

    public void signInUser(String emailId, String password) {

        if (TextUtils.isEmpty(emailId)){
            getView().showMessage(R.string.enter_emailid_msg, null, true);
        }else if (!GeneralFunctions.isValidEmail(emailId)) {
            getView().showMessage(R.string.enter_email, null, true);
        } else if (TextUtils.isEmpty(password)) {
            getView().showMessage(R.string.enter_password_msg, null, true);
        } else if (!GeneralFunctions.isValidPassword(password)) {
            getView().showMessage(R.string.invalid_password, null, true);
        } else {
            getView().showProgressLoader();
            mCompositeDisposable.add(mSignInInteractor.signInUser(emailId, password,
                    new NetworkRequestCallbacks() {
                        @Override
                        public void onSuccess(Response<?> response) {
                            if (null != getView() && !getView().isFragmentDestroyed()) {
                                getView().hideProgressLoader();
                                try {
                                    PojoNetworkResponse pojoNetworkResponse = RetrofitRequest
                                            .checkForResponseCode(response.code());
                                    if (pojoNetworkResponse.isSuccess() && null != response.body()) {
                                        PojoUserSignIn pojoUserSignIn = (PojoUserSignIn) response.body();
                                        getView().getUserPrefsManager()
                                                .saveUserProfile(true,
                                                        pojoUserSignIn.getSession_id(),
                                                        pojoUserSignIn.getProfile());
                                        getView().navigateToHomeScreen();

                                    } else if (pojoNetworkResponse.isSessionExpired()) {
                                        getView().expireUserSession();
                                    } else {
                                        getView().showMessage(0, RetrofitRequest
                                                        .getErrorMessage(response.errorBody()),
                                                true);
                                    }
                                } catch (Exception e) {
                                    getView().hideProgressLoader();
                                    e.printStackTrace();
                                    getView().showMessage(R.string.retrofit_failure,
                                            null, true);
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            if (null != getView() && !getView().isFragmentDestroyed()) {
                                getView().hideProgressLoader();
                                getView().showMessage(RetrofitRequest.getRetrofitError(t),
                                        null, true);
                            }
                        }
                    }));
        }
    }

    public void forgotPassword(String emailId) {
        if (!GeneralFunctions.isValidEmail(emailId)) {
            getView().showMessage(R.string.enter_email, null,
                    false);
        } else {
            getView().showProgressLoader();
            mCompositeDisposable.add(mSignInInteractor.forgotPassword(emailId,
                    new NetworkRequestCallbacks() {
                        @Override
                        public void onSuccess(Response<?> response) {
                            if (null != getView() && !getView().isFragmentDestroyed()) {
                                getView().hideProgressLoader();
                                try {
                                    PojoNetworkResponse pojoNetworkResponse = RetrofitRequest
                                            .checkForResponseCode(response.code());
                                    if (pojoNetworkResponse.isSuccess() && null != response.body()) {
                                        getView().showMessage(0, ((PojoCommon) response.body()).
                                                getMessage(), false);
                                        getView().dismissDialogFragment();

                                    } else if (pojoNetworkResponse.isSessionExpired()) {
                                        getView().expireUserSession();
                                    } else {
                                        getView().showMessage(0, RetrofitRequest
                                                        .getErrorMessage(response.errorBody()),
                                                false);
                                    }
                                } catch (Exception e) {
                                    getView().hideProgressLoader();
                                    e.printStackTrace();
                                    getView().showMessage(R.string.retrofit_failure,
                                            null, true);
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            if (null != getView() && !getView().isFragmentDestroyed()) {
                                getView().hideProgressLoader();
                                getView().showMessage(RetrofitRequest.getRetrofitError(t),
                                        null, true);
                            }
                        }
                    }));
        }
    }

    @Override public void detachView() {
        mSignInInteractor = null;
        super.detachView();
    }
}
