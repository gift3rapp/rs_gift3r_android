package com.richestsoft.gift3r.presenters;

import android.app.Dialog;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.interactors.StoreDetailInteractor;
import com.richestsoft.gift3r.models.networkrequest.NetworkRequestCallbacks;
import com.richestsoft.gift3r.models.networkrequest.RetrofitRequest;
import com.richestsoft.gift3r.models.pojos.PojoNetworkResponse;
import com.richestsoft.gift3r.views.StoreDetailView;
import com.richestsoft.gift3r.views.activities.HomeActivity;
import com.richestsoft.gift3r.views.adapters.StoreDetailAdapter;
import com.richestsoft.gift3r.views.fragments.StoreDetailFragment;

import retrofit2.Response;

/**
 * Created by user28 on 9/2/18.
 */

public class StoreDetailPresenter extends BasePresenter<StoreDetailView> {

    private StoreDetailInteractor mStoreDetailInteractor;

    @Override public void attachView(StoreDetailView view) {
        super.attachView(view);
        mStoreDetailInteractor = new StoreDetailInteractor();
    }

    public void buyMyCard(Integer cardId, String phoneNumber,String message,int payment_id) {
   //     Log.e("phone number in api",phoneNumber);
        getView().showProgressLoader();
        mCompositeDisposable.add(mStoreDetailInteractor.buyMyCard(cardId, phoneNumber,message,payment_id,
                new NetworkRequestCallbacks() {
                    @Override
                    public void onSuccess(Response<?> response) {
                        if (null != getView() && !getView().isFragmentDestroyed()) {
                            getView().hideProgressLoader();
                            try {
                                PojoNetworkResponse pojoNetworkResponse = RetrofitRequest
                                        .checkForResponseCode(response.code());
                                if (pojoNetworkResponse.isSuccess() && null != response.body()) {

                                    getView().sendBroadcast();

                                    Show_Thankyou_Page();


                                } else if (pojoNetworkResponse.isSessionExpired()) {
                                    getView().expireUserSession();
                                } else {
                                    getView().showMessage(0, RetrofitRequest
                                                    .getErrorMessage(response.errorBody()),
                                            true);
                                }
                            } catch (Exception e) {
                                getView().hideProgressLoader();
                                e.printStackTrace();
                                getView().showMessage(R.string.retrofit_failure,
                                        null, true);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        if (null != getView() && !getView().isFragmentDestroyed()) {
                            getView().hideProgressLoader();
                            getView().showMessage(RetrofitRequest.getRetrofitError(t),
                                    null, true);
                        }
                    }
                }));
    }

    @Override public void detachView() {
        mStoreDetailInteractor = null;
        super.detachView();
    }

    private void Show_Thankyou_Page() {

        final Dialog dialog = new Dialog(getView().getActivityContext());
        dialog.setContentView(R.layout.thankupagelayout);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCanceledOnTouchOutside(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);
        // set the custom dialog components - text, image and button
        TextView btn_view_gift = (TextView) dialog.findViewById(R.id.btn_view_gift);

        btn_view_gift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getView().getActivityContext(), HomeActivity.class);
                intent.putExtra("type","buycards");
                getView().getActivityContext().startActivity(intent);
                StoreDetailFragment.activity.finish();

            }
        });

        dialog.show();

    }


}
