package com.richestsoft.gift3r.presenters;

import android.app.Dialog;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.interactors.MyCardsInteractor;
import com.richestsoft.gift3r.models.networkrequest.NetworkRequestCallbacks;
import com.richestsoft.gift3r.models.networkrequest.RetrofitRequest;
import com.richestsoft.gift3r.models.pojos.GiftCard;
import com.richestsoft.gift3r.models.pojos.PojoNetworkResponse;
import com.richestsoft.gift3r.views.MyCardsView;

import java.util.ArrayList;

import retrofit2.Response;

/**
 * Created by user28 on 9/2/18.
 */

public class MyCardsPresenter extends BasePresenter<MyCardsView> {

    private MyCardsInteractor mMyCardsInteractor;

    @Override public void attachView(MyCardsView view) {
        super.attachView(view);
        mMyCardsInteractor = new MyCardsInteractor();
    }

    public void fetchMyCardList(Boolean isShowSwipeRefreshLoader, String searchKey) {
        if (isShowSwipeRefreshLoader) {
            getView().showSwipeRefreshLoader();
        }
        mCompositeDisposable.add(mMyCardsInteractor.getMyCardsLists(searchKey, new NetworkRequestCallbacks() {
            @Override
            public void onSuccess(Response<?> response) {
                if (null != getView() && !getView().isFragmentDestroyed()) {
                    getView().hideSwipeRefreshLoader();
                    try {
                        PojoNetworkResponse pojoNetworkResponse = RetrofitRequest
                                .checkForResponseCode(response.code());
                        if (pojoNetworkResponse.isSuccess() && null != response.body()) {
                            updateData((ArrayList<GiftCard>) response.body());

                        } else if (pojoNetworkResponse.isSessionExpired()) {
                            getView().expireUserSession();
                        } else {
                            getView().showMessage(0, RetrofitRequest
                                    .getErrorMessage(response.errorBody()), true);
                        }
                    } catch (Exception e) {
                        getView().hideSwipeRefreshLoader();
                        e.printStackTrace();
                        getView().showMessage(R.string.retrofit_failure,
                                null, true);
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
                if (null != getView() && !getView().isFragmentDestroyed()) {
                    getView().hideSwipeRefreshLoader();
                    getView().showMessage(RetrofitRequest.getRetrofitError(t),
                            null, true);
                }
            }
        }));
    }

    private void updateData(ArrayList<?> userList) {
        getView().updateRecyclerViewData(userList);
        if (0 < userList.size()) {
            getView().showNoDataText(0, "");
        } else {
            getView().showNoDataText(R.string.no_card_found, null);
        }
    }

    public void sendMyCard(Integer cardId,String purchasedcard_id, String phoneNumber,String message) {
        getView().showProgressLoader();
        mCompositeDisposable.add(mMyCardsInteractor.sendMyCard(cardId,purchasedcard_id, phoneNumber,message,
                new NetworkRequestCallbacks() {
                    @Override
                    public void onSuccess(Response<?> response) {
                        if (null != getView() && !getView().isFragmentDestroyed()) {
                            getView().hideProgressLoader();
                            try {
                                PojoNetworkResponse pojoNetworkResponse = RetrofitRequest
                                        .checkForResponseCode(response.code());
                                if (pojoNetworkResponse.isSuccess() && null != response.body()) {

                                    getView().sendBroadcast();
                                    Toast.makeText(getView().getActivityContext(), "card sent successfully", Toast.LENGTH_SHORT).show();
                                //    Show_Thankyou_Page();

                                } else if (pojoNetworkResponse.isSessionExpired()) {
                                    getView().expireUserSession();
                                } else {
                                    getView().showMessage(0, RetrofitRequest
                                                    .getErrorMessage(response.errorBody()),
                                            true);
                                }
                            } catch (Exception e) {
                                getView().hideProgressLoader();
                                e.printStackTrace();
                                getView().showMessage(R.string.retrofit_failure,
                                        null, true);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        if (null != getView() && !getView().isFragmentDestroyed()) {
                            getView().hideProgressLoader();
                            getView().showMessage(RetrofitRequest.getRetrofitError(t),
                                    null, true);
                        }
                    }
                }));
    }

    @Override public void detachView() {
        mMyCardsInteractor = null;
        super.detachView();
    }
}
