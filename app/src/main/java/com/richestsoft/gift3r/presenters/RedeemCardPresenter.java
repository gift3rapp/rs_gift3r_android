package com.richestsoft.gift3r.presenters;

import android.util.Log;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.views.RedeemCardView;

/**
 * Created by user28 on 13/2/18.
 */

public class RedeemCardPresenter extends BasePresenter<RedeemCardView> {

    public void proceedToPaymentConfirmation(Integer cardId, String purchasecardid,String amount, String receiptNo,String receiptName) {


        if (amount.isEmpty()) {
            getView().showMessage(R.string.enter_the_total_amount, null, false);
        } else if (receiptNo.length() != 4) {
            getView().showMessage(R.string.enter_the_last_four_digits_of_your_receipt_no_toast, null, false);
        }else if (receiptName.isEmpty()) {
            getView().showMessage(R.string.enter_the_last_four_digits_of_your_receipt_name_toast, null, false);
        } else {
            getView().proceedToConfirmation(cardId, purchasecardid,amount, receiptNo,receiptName);
        }
    }
}
