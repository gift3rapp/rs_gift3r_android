package com.richestsoft.gift3r.presenters;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.interactors.OtpVerificationInteractor;
import com.richestsoft.gift3r.models.networkrequest.NetworkRequestCallbacks;
import com.richestsoft.gift3r.models.networkrequest.RetrofitRequest;
import com.richestsoft.gift3r.models.pojos.PojoNetworkResponse;
import com.richestsoft.gift3r.models.pojos.PojoUserSignIn;
import com.richestsoft.gift3r.views.OtpVerificationView;

import retrofit2.Response;

/**
 * Created by user28 on 8/2/18.
 */

public class OtpVerificationPresenter extends BasePresenter<OtpVerificationView> {

    private OtpVerificationInteractor mOtpVerificationInteractor;

    @Override public void attachView(OtpVerificationView view) {
        super.attachView(view);
        mOtpVerificationInteractor = new OtpVerificationInteractor();
    }

    //sign up user
    public void signUpUser(String email, String countryCode, String phoneNumber,
                           String name, String password, String otp) {
        if (otp.isEmpty()) {
            getView().showMessage(R.string.enter_otp, null, true);
        } else {
            getView().showProgressLoader();
            mCompositeDisposable.add(mOtpVerificationInteractor.signUpUser(email, countryCode, phoneNumber, name, password, otp, new NetworkRequestCallbacks() {
                        @Override
                        public void onSuccess(Response<?> response) {
                            if (null != getView() && !getView().isFragmentDestroyed()) {
                                try {
                                    getView().hideProgressLoader();
                                    PojoNetworkResponse pojoNetworkResponse = RetrofitRequest
                                            .checkForResponseCode(response.code());
                                    if (pojoNetworkResponse.isSuccess() && null != response.body()) {
                                        PojoUserSignIn pojoUserSignIn = (PojoUserSignIn) response.body();
                                        getView().getUserPrefsManager()
                                                .saveUserProfile(true, pojoUserSignIn.getSession_id(), pojoUserSignIn.getProfile());
                                        getView().navigateToHomeScreen();
                                    } else if (pojoNetworkResponse.isSessionExpired()) {
                                        getView().expireUserSession();
                                    } else {
                                        getView().showMessage(0, RetrofitRequest
                                                .getErrorMessage(response.errorBody()), true);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    getView().showMessage(R.string.retrofit_failure,
                                            null, true);
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            if (null != getView() && !getView().isFragmentDestroyed()) {
                                getView().hideProgressLoader();
                                getView().showMessage(RetrofitRequest.getRetrofitError(t),
                                        null, true);
                            }
                        }
                    }));
        }
    }

    @Override public void detachView() {
        mOtpVerificationInteractor = null;
        super.detachView();
    }
}
