package com.richestsoft.gift3r.presenters;

import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.interactors.StoreListInteractor;
import com.richestsoft.gift3r.models.networkrequest.NetworkRequestCallbacks;
import com.richestsoft.gift3r.models.networkrequest.RetrofitRequest;
import com.richestsoft.gift3r.models.pojos.PojoNetworkResponse;
import com.richestsoft.gift3r.models.pojos.Store;
import com.richestsoft.gift3r.views.StoreListView;
import com.richestsoft.gift3r.views.fragments.StoreListFragment;

import java.util.ArrayList;

import retrofit2.Response;

/**
 * Created by user28 on 8/2/18.
 */

public class StoreListPresenter extends BasePresenter<StoreListView> {

    private StoreListInteractor mStoreListInteractor;

    @Override public void attachView(StoreListView view) {
        super.attachView(view);
        mStoreListInteractor = new StoreListInteractor();
    }

    public void fetchMyCardList(Boolean isShowSwipeRefreshLoader, String searchKey, Integer page) {
        if (isShowSwipeRefreshLoader) {
            getView().showSwipeRefreshLoader();
        }
        mCompositeDisposable.add(mStoreListInteractor.getStoreList(searchKey, StoreListFragment.STORES_PAGE_LIMIT, page, new NetworkRequestCallbacks() {
            @Override
            public void onSuccess(Response<?> response) {
                if (null != getView() && !getView().isFragmentDestroyed()) {
                    getView().hideSwipeRefreshLoader();
                    try {
                        PojoNetworkResponse pojoNetworkResponse = RetrofitRequest
                                .checkForResponseCode(response.code());
                        if (pojoNetworkResponse.isSuccess() && null != response.body()) {
                            updateData((ArrayList<Store>) response.body());
                        } else if (pojoNetworkResponse.isSessionExpired()) {
                            getView().expireUserSession();
                        } else {
                            getView().showMessage(0, RetrofitRequest
                                    .getErrorMessage(response.errorBody()), true);
                        }
                    } catch (Exception e) {
                        getView().hideSwipeRefreshLoader();
                        e.printStackTrace();
                        getView().showMessage(R.string.retrofit_failure,
                                null, true);
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
                if (null != getView() && !getView().isFragmentDestroyed()) {
                    getView().hideSwipeRefreshLoader();
                    getView().showMessage(RetrofitRequest.getRetrofitError(t),
                            null, true);
                }
            }
        }));
    }

    private void updateData(ArrayList<?> userList) {
        getView().updateRecyclerViewData(userList);
        if (0 < userList.size()) {
            getView().showNoDataText(0, "");
        } else {
            getView().showNoDataText(R.string.no_store_found, null);
        }
    }

    @Override public void detachView() {
        mStoreListInteractor = null;
        super.detachView();
    }
}
