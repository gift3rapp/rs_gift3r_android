package com.richestsoft.gift3r.utils;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.firebase.FirebaseApp;
import com.instabug.library.Instabug;
import com.instabug.library.InstabugColorTheme;
import com.instabug.library.InstabugCustomTextPlaceHolder;
import com.instabug.library.internal.module.InstabugLocale;
import com.instabug.library.invocation.InstabugInvocationEvent;
import com.instabug.library.ui.onboarding.WelcomeMessage;
import com.richestsoft.gift3r.R;
import com.richestsoft.gift3r.models.preferences.UserPrefsManager;

import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Mukesh on 05-05-2016.
 */

public class ApplicationGlobal extends Application {

    private static String sessionId = "";
    private static String deviceLocale = "";
    private static String firebasetoken = "";

    @Override
    public void onCreate() {
        super.onCreate();

       // helper.DeleteData();

     //   checkpermissions(getApplicationContext());

        Instabug();

        FirebaseApp.initializeApp(this);
        Fresco.initialize(this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.font_regular))
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        // get device locale
        deviceLocale = Locale.getDefault().getLanguage();

        // get session id
        UserPrefsManager userPrefsManager = new UserPrefsManager(this);
        if (userPrefsManager.getIsLogined()) {
            sessionId = userPrefsManager.getSessionId();
        }

        sessionId = new UserPrefsManager(this).getSessionId();
    }

    public static String getSessionId() {
        return sessionId;
    }

    public static void setSessionId(String sessionId) {
        ApplicationGlobal.sessionId = sessionId;
    }

    public static String getDeviceLocale() {
        return deviceLocale;
    }

    public static String getFirebasetoken() {
        return firebasetoken;
    }

    public static void setFirebasetoken(String firebasetoken) {

        ApplicationGlobal.firebasetoken = firebasetoken;

    }

    public void Instabug(){

    /*    new Instabug.Builder(this, "7f006e8e013cdc14d23f767d6c623efa")
                .setInvocationEvents(InstabugInvocationEvent.SHAKE, InstabugInvocationEvent.SCREENSHOT,
                        InstabugInvocationEvent.FLOATING_BUTTON, InstabugInvocationEvent.TWO_FINGER_SWIPE_LEFT)
                .build();
*/

        new Instabug.Builder(this, "7e24d93bd587236293de75e75816ea7d")
                .setInvocationEvents(InstabugInvocationEvent.SHAKE, InstabugInvocationEvent.SCREENSHOT,
                        InstabugInvocationEvent.FLOATING_BUTTON, InstabugInvocationEvent.TWO_FINGER_SWIPE_LEFT)
                .build();


        //Choosing instabug theme
        Instabug.setColorTheme(InstabugColorTheme.InstabugColorThemeLight);
        //Choosing type of attachments allowed
        //1. initial screenshot, 2. extra screenshot, 3. image from gallery, 4. voice note
        //5. screen record
        Instabug.setWelcomeMessageState(WelcomeMessage.State.BETA);

        // TODO the following are 3 acceptable ways to force Locale in Instabug (last one is the only 1 applied)
        Instabug.setLocale(new Locale(InstabugLocale.SIMPLIFIED_CHINESE.getCode(),
                InstabugLocale.SIMPLIFIED_CHINESE.getCountry()));
        Instabug.setLocale(new Locale(InstabugLocale.FRENCH.getCode()));
        Instabug.setLocale(Locale.ENGLISH);

        //To show instabug debug logs if necessary
        Instabug.setDebugEnabled(true);

        //Settings custom strings to replace instabug's strings
        InstabugCustomTextPlaceHolder placeHolder = new InstabugCustomTextPlaceHolder();
        placeHolder.set(InstabugCustomTextPlaceHolder.Key.REPORT_FEEDBACK, "Send Feedback");
        placeHolder.set(InstabugCustomTextPlaceHolder.Key.REPORT_BUG, "Send Bug Report");
        Instabug.setCustomTextPlaceHolders(placeHolder);

        //setting user attributes
        Instabug.setUserAttribute("USER_TYPE", "instabug user");

        Instabug.setAutoScreenRecordingEnabled(true);
    }

    public static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}