package com.richestsoft.gift3r.utils;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by Mukesh on 06-06-2016.
 */
public class ScreenDimensions {

    private static int screenWidth, screenHeight;
    private Context mContext;

    public ScreenDimensions(Context context) {
        mContext = context;
    }

    public int getScreenWidth() {
        if (screenWidth == 0) {
            WindowManager wm = (WindowManager) mContext
                    .getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            if (android.os.Build.VERSION.SDK_INT >= 13) {
                Point size = new Point();
                display.getSize(size);
                screenWidth = size.x;
            } else {
                screenWidth = display.getWidth();
            }
            return screenWidth;
        } else {
            return screenWidth;
        }
    }

    public int getScreenHeight() {
        if (screenHeight == 0) {
            WindowManager wm = (WindowManager) mContext
                    .getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            if (android.os.Build.VERSION.SDK_INT >= 13) {
                Point size = new Point();
                display.getSize(size);
                screenHeight = size.y;
            } else {
                screenHeight = display.getHeight();
            }
            return screenHeight;
        } else {
            return screenHeight;
        }
    }

    public int getHeightWithGivenPercentage(int percentage) {
        if (screenHeight == 0) {
            screenHeight = getScreenHeight();
        }
        return (percentage * screenHeight) / 100;
    }
}
